#!/usr/bin/env bash

sudo apt-get -y install squid
sudo mv /etc/squid/squid.conf /etc/squid/squid.conf.orig
sudo wget -q https://framagit.org/underxctrl/wifilter/-/raw/main/config%2Fsquid.conf -O /etc/squid/squid.conf
sudo apt-get -y install squidguard
sudo mv /etc/squidguard/squidGuard.conf /etc/squidguard/squidGuard.conf.orig
sudo wget -q https://framagit.org/underxctrl/wifilter/-/raw/main/wifi_dongle%2Fconfig%2FsquidGuard.conf -O /etc/squidguard/squidGuard.conf
sudo mkdir -p /var/www/html/block/img
sudo wget -q https://framagit.org/underxctrl/wifilter/-/raw/main/wifi_dongle%2Fconfig%2Fblock%2Findex.html -O /var/www/html/block/index.html
sudo wget -q https://framagit.org/underxctrl/wifilter/-/raw/main/config%2Fblock%2Fimg%2F0108_A_matrix.jpg -O /var/www/html/block/img/0108_A_matrix.jpg
sudo chown -R www-data:www-data /var/www/html/block
sudo mv /etc/lighttpd/lighttpd.conf /etc/lighttpd/lighttpd.conf.orig
sudo wget -q https://framagit.org/underxctrl/wifilter/-/raw/main/config%2Flighttpd.conf -O /etc/lighttpd/lighttpd.conf
sudo mv /etc/lighttpd/conf-available/50-raspap-router.conf /etc/lighttpd/conf-available/50-raspap-router.conf.orig
sudo wget -q https://framagit.org/underxctrl/wifilter/-/raw/main/config%2F50-raspap-router.conf -O /etc/lighttpd/conf-available/50-raspap-router.conf
sudo systemctl restart lighttpd
curl -sL https://framagit.org/underxctrl/wifilter/-/raw/main/wifi_dongle%2Fconfig%2Fiptables.sh | sudo bash
wget -q https://framagit.org/underxctrl/wifilter/-/raw/main/update_squidguard.sh -O ~/update_squidguard.sh
crontab -l > curcron
echo "0 2 * * * bash /home/pi/update_squidguard.sh" >> curcron
crontab curcron
rm curcron
bash ~/update_squidguard.sh
