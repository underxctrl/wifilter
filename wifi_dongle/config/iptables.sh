#!/usr/bin/env bash

# Clear all rules
iptables -F
iptables -t nat -F

# Whitelist mode
iptables -P INPUT ACCEPT
iptables -P FORWARD DROP
iptables -P OUTPUT ACCEPT

# Allow NAT
iptables -t nat -A POSTROUTING -o wlan1 -j MASQUERADE
iptables -A FORWARD -i wlan1 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT

mv /etc/iptables/rules.v4 /etc/iptables/rules.v4.orig
bash -c "iptables-save > /etc/iptables/rules.v4"
