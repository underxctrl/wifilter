#!/usr/bin/env bash

curl -sL https://framagit.org/underxctrl/wifilter/-/raw/main/install_raspap_2.8.5.sh | bash
curl -sL https://framagit.org/underxctrl/wifilter/-/raw/main/wifi_dongle%2Finstall_squidguard.sh | bash
curl -sL https://framagit.org/underxctrl/wifilter/-/raw/main/wifi_dongle%2Fsetup_wifi_dongle.sh | bash
