#!/usr/bin/env bash

cd /etc/wpa_supplicant/
sudo cp wpa_supplicant.conf wpa_supplicant.conf.orig
sudo mv wpa_supplicant.conf wpa_supplicant-wlan1.conf
cd
sudo cp /etc/dhcpcd.conf /etc/dhcpcd.conf.orig
sudo wget -q https://framagit.org/underxctrl/wifilter/-/raw/main/wifi_dongle%2Fconfig%2Fdhcpcd.conf -O /etc/dhcpcd.conf
echo "www-data ALL=(ALL) NOPASSWD:/sbin/wpa_cli -i wlan1 scan_results" | sudo EDITOR="tee -a" visudo
echo "www-data ALL=(ALL) NOPASSWD:/sbin/wpa_cli -i wlan1 scan" | sudo EDITOR="tee -a" visudo
