#!/usr/bin/env bash

sudo apt-get -y install python3-pip
sudo apt-get -y install python3-venv
pip3 install --user pipx
python3 -m pipx ensurepath
eval "$(cat ~/.bashrc | tail -n 1)"
pipx install mitmproxy
pipx inject mitmproxy scikit-learn==1.0.2
sudo apt-get -y install chromium-driver xvfb
pipx inject mitmproxy pyvirtualdisplay
pipx inject mitmproxy selenium==4.10.0
pipx inject mitmproxy langdetect
sudo wget -q https://framagit.org/underxctrl/wifilter/-/raw/main/config%2Fmlfilter%2Fsquid.conf -O /etc/squid/squid.conf
sudo systemctl restart squid
wget -q https://framagit.org/underxctrl/wifilter/-/raw/main/config%2Fmlfilter%2Fwifi_dongle%2Fblock-urls.py -O ~/block-urls.py
wget -q https://framagit.org/underxctrl/wifilter/-/raw/main/config%2Fmlfilter%2Fadult_model_objects.pkl -O ~/adult_model_objects.pkl
wget -q https://framagit.org/underxctrl/wifilter/-/raw/main/config%2Fmlfilter%2Fstart_mitmdump.py -O ~/start_mitmdump.py
chmod +x start_mitmdump.py
crontab -l > curcron
echo "@reboot /home/pi/start_mitmdump.py" >> curcron
crontab curcron
rm curcron
wget -q https://framagit.org/underxctrl/wifilter/-/raw/main/config%2Fmlfilter%2Fwhitelist.txt -O ~/whitelist.txt
touch adult_hosts.txt
touch unauth_lang_hosts.txt
./start_mitmdump.py
