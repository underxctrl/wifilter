#!/usr/bin/env python3

import sys
import os

root_dir = '/home/pi/'

log = False
if len(sys.argv) > 1 and \
   sys.argv[1] == 'log':
    log = True

whitelist = []
with open(root_dir + 'whitelist.txt') as f:
    for line in f:
        if line[0] != '#':
            whitelist += [line.strip()]

command = root_dir + '.local/bin/mitmdump -s ' + root_dir + 'block-urls.py --ignore-hosts ' + ' --ignore-hosts '.join(whitelist)

if log:
    command += ' > mitmdump.log'

print(command)
os.system(command)
