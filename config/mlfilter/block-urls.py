from mitmproxy import http
from mitmproxy import ctx

from selenium import webdriver
from selenium.webdriver.common.by import By
from pyvirtualdisplay import Display
import gc
import base64
import re
from langdetect import detect, DetectorFactory
DetectorFactory.seed = 0
import pickle

ap_ip_address = '192.168.50.1'
#ap_ip_address = '10.3.141.1'

class BlockResource:
    def __init__(self):
        self.whitelist = []
        with open('/home/pi/whitelist.txt') as f:
            for line in f:
                if line[0] != '#':
                    self.whitelist += [line.strip()]
        with open('/home/pi/adult_model_objects.pkl', 'rb') as f:
            model_dict = pickle.load(f)
            self.en_count_vect = model_dict['en'][0]
            self.en_tfidf_transformer = model_dict['en'][1]
            self.en_clf = model_dict['en'][2]
            self.fr_count_vect = model_dict['fr'][0]
            self.fr_tfidf_transformer = model_dict['fr'][1]
            self.fr_clf = model_dict['fr'][2]

        self.display = Display(visible=0, size=(800, 600))
        self.display.start()

        self.driver = webdriver.Chrome()
        self.text = ''

        self.adult_hosts = []
        with open('/home/pi/adult_hosts.txt') as f:
            for line in f:
                host = line.strip()
                self.adult_hosts += [host]
        self.adult_host_file = open('/home/pi/adult_hosts.txt', 'a', 1)
        self.banned_words = ['fuck', 'hd', 'hentai', 'porn', 'sex', 'xx']
        self.unauth_lang_hosts = []
        with open('/home/pi/unauth_lang_hosts.txt') as f:
            for line in f:
                host = line.strip()
                self.unauth_lang_hosts += [host]
        self.unauth_lang_host_file = open('/home/pi/unauth_lang_hosts.txt', 'a', 1)

    def clean_text(self):
        new_lines = []
        lines = self.text.split('\n')
        lines = [line for line in lines if line]
        for line in lines:
            new_line = ''
            for w in line.split():
                if w.isalpha() and len(w) > 2:
                    if new_line:
                        new_line += ' ' + w
                    else:
                        new_line = w
            new_lines += [new_line]
        self.text = '\n'.join(new_lines)

    def test_domain_name(self, host):
        for w in self.banned_words:
            if w in host:
                return True
        return False

    def requestheaders(self, flow):
        gc.collect()
        if flow.request.host == ap_ip_address:
            return 0
        host = '.'.join(flow.request.pretty_host.split('.')[-2:])
        if host in self.whitelist:
            return 0
        if host in self.adult_hosts:
            ctx.log.info('** Adult site **')
            flow.response = http.Response.make(308, b'', {'Location': 'http://' + ap_ip_address + ':8000/block/'})
            return 0
        if self.test_domain_name(host):
            ctx.log.info('** Adult site **')
            self.adult_hosts += [host]
            self.adult_host_file.write(host + '\n')
            flow.response = http.Response.make(308, b'', {'Location': 'http://' + ap_ip_address + ':8000/block/'})
            return 0
        if host in self.unauth_lang_hosts:
            ctx.log.info('** Unauthorized language site **')
            flow.response = http.Response.make(308, b'', {'Location': 'http://' + ap_ip_address + ':8000/block/'})
            return 0
        if 'content-type' in flow.request.headers:
            ct = flow.request.headers['content-type']
        elif 'accept' in flow.request.headers:
            ct = flow.request.headers['accept']
        else:
            ct = ''
        if 'html' in ct:
            ctx.log.info('*** Processing ' + flow.request.pretty_url + '...')
            self.driver.get(flow.request.pretty_url)
            self.text = self.driver.find_element(By.TAG_NAME, 'html').text
            self.clean_text()
            if len(re.findall(r'\w+', self.text)) < 30: # word token number limit
                return 0
            try:
                lang = detect(self.text)
            except:
                lang = 'none'

            if lang == 'en':
                X_test_counts = self.en_count_vect.transform([self.text])
                X_test_tfidf = self.en_tfidf_transformer.transform(X_test_counts)
                predicted = self.en_clf.predict(X_test_tfidf)
                if predicted[0] < 1:
                    ctx.log.info('** Adult site **')
                    self.adult_hosts += [host]
                    self.adult_host_file.write(host + '\n')
                    flow.response = http.Response.make(308, b'', {'Location': 'http://' + ap_ip_address + ':8000/block/'})
            elif lang == 'fr':
                X_test_counts = self.fr_count_vect.transform([self.text])
                X_test_tfidf = self.fr_tfidf_transformer.transform(X_test_counts)
                predicted = self.fr_clf.predict(X_test_tfidf)
                if predicted[0] < 1:
                    ctx.log.info('** Adult site **')
                    self.adult_hosts += [host]
                    self.adult_host_file.write(host + '\n')
                    flow.response = http.Response.make(308, b'', {'Location': 'http://' + ap_ip_address + ':8000/block/'})
            else: # deny any other language
                ctx.log.info('** Unauthorized language site **')
                self.unauth_lang_hosts += [host]
                self.unauth_lang_host_file.write(host + '\n')
                flow.response = http.Response.make(308, b'', {'Location': 'http://' + ap_ip_address + ':8000/block/'})

        return 0

    def done(self):
        self.adult_host_file.close()
        self.unauth_lang_host_file.close()

addons = [BlockResource()]
