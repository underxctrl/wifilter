#!/usr/bin/env bash

curl -sL https://framagit.org/underxctrl/wifilter/-/raw/main/install_raspap_2.8.5.sh | bash
curl -sL https://framagit.org/underxctrl/wifilter/-/raw/main/install_squidguard.sh | bash
