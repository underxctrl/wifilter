# Introduction

The purpose of WiFilter is to allow filtering of any WiFi network based on a wireless router ([RaspAP](https://raspap.com/)) extended to run a proxy filter ([squidGuard](http://www.squidguard.org/)).<br>
By default, WiFilter filters adult sites based on the [Université Toulouse 1 Capitole blacklist](https://dsi.ut-capitole.fr/blacklists/index_en.php).<br>
WiFilter has been tested on Raspberry Pi 4 Model B. 

# Install

1. Begin by flashing an SD card with the **Debian Bullseye** release of [Raspberry Pi OS (64-bit) Lite](https://www.raspberrypi.org/downloads/raspbian/) using Raspberry Pi Imager with advanced options:
    - Set hostname `raspberrypi.local`
    - Enable SSH
    - Set username and password
    - Configure wireless LAN & set wireless LAN country
    - Set locale settings
2. Insert the SD card into the Raspberry Pi and connect it to power. Note: the standard power supply for the Raspberry Pi 4 Model B is 5.1V @ 3A. Other power sources may result in undervoltage or other issues.
3. Connect to your Pi via ssh: `ssh pi@raspberrypi.local` 
4. Update RPi OS to its latest version, including the kernel and firmware, followed by a reboot:
```
sudo apt-get update
sudo apt-get full-upgrade
sudo reboot
```
5. Skip to the **External wireless adapter section** if you have a USB WiFi dongle, otherwise reconnect to your Pi and install WiFilter: `curl -sL https://framagit.org/underxctrl/wifilter/-/raw/main/install_wifilter.sh | bash`.
6. Once the installation is complete, open the RaspAP admin interface in your browser, usually http://raspberrypi.local:8000.
7. Log in as *admin* using the password *secret*.
8. Change the admin password using the **Authentication** section.
9. Change the hotspot password using the **Hotspot > Security** panel.
10. Choose **Hotspot > Advanced**, set the `Country Code` parameter at the bottom of the page to match your wireless LAN country before enabling the **WiFi client AP mode** option.
11. Optionally, enable **Logfile output** as this is often helpful for troubleshooting.
12. Choose **Save settings** and **Start hotspot**.
13. Wait a few moments and confirm that your access point has started.
14. You can now connect to the wireless network with SSID `raspi-webgui` and the hotspot password you set previously. For your connection to work, you have to set a proxy server at IP address `192.168.50.1` and port `3128`. If you try to connect to for example <ins>youporn.com</ins>, you should get [-ACCESS DENIED-] (HTTP) or [-connection failed-] (HTTPS). But if you try to connect to for example <ins>wikipedia.org</ins>, you should get [+access+].

# External wireless adapter (to prevent 2.4GHz/5GHz radio interference)

Tested with a Ralink Technology, Corp. MT7601U Wireless Adapter (USB WiFi dongle).

1. Once you have completed steps 1 - 4 in the **Install section**, reconnect to your Pi and install WiFilter: `curl -sL https://framagit.org/underxctrl/wifilter/-/raw/main/wifi_dongle/install_wifilter.sh | bash`.
2. Once the installation is complete, reboot:
```
sudo reboot
```
3. Reconnect to your Pi and open the RaspAP admin interface in your browser, usually http://raspberrypi.local:8000.
4. Log in as *admin* using the password *secret*.
5. Change the admin password using the **Authentication** section.
6. Change the hotspot password using the **Hotspot > Security** panel.
7. Choose **Hotspot > Advanced**, set the `Country Code` parameter at the bottom of the page to match your wireless LAN country.
8. Optionally, enable **Logfile output** as this is often helpful for troubleshooting.
9. Choose **Save settings** and reboot:
```
sudo reboot
```
10. When your access point is back online, you can now connect to the wireless network with SSID `raspi-webgui` and the hotspot password you set previously. For your connection to work, you have to set a proxy server at IP address `10.3.141.1` and port `3128`. If you try to connect to for example <ins>youporn.com</ins>, you should get [-ACCESS DENIED-] (HTTP) or [-connection failed-] (HTTPS). But if you try to connect to for example <ins>wikipedia.org</ins>, you should get [+access+].

# Optional: advanced system (experimental)

A machine learning model has been trained to filter out English and French adult sites that are not on the [Université Toulouse 1 Capitole blacklist](https://dsi.ut-capitole.fr/blacklists/index_en.php).
Proceed to this step after you have installed the basic system (see [Install](#install) section).

1. Connect to your Raspberry Pi: `ssh pi@192.168.50.1` or `ssh pi@10.3.141.1` (with external wireless adapter)
2. Install MLFilter: `curl -sL https://framagit.org/underxctrl/wifilter/-/raw/main/install_mlfilter.sh | bash` or `curl -sL https://framagit.org/underxctrl/wifilter/-/raw/main/wifi_dongle/install_mlfilter.sh | bash` (with external wireless adapter)
3. Adapt `/home/pi/whitelist.txt` to your needs (one domain per line) to avoid unnecessary filter processing for trusted domains.
4. Configure your browser or device installing the Certificate Authority: visit http://mitm.it and follow the instructions for your OS / system.

# License

See the [LICENSE](https://framagit.org/underxctrl/wifilter/-/raw/main/LICENSE) file.
