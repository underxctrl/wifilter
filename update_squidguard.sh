#!/usr/bin/env bash

sudo chown -R pi:pi /var/lib/squidguard/db*
rsync -arpogvt --delete rsync://ftp.ut-capitole.fr/blacklist/dest/adult /var/lib/squidguard/db/
sudo squidGuard -C all
sudo chown -R proxy:proxy /var/lib/squidguard/db*
sudo chmod -R 777 /var/lib/squidguard/db*
sudo systemctl restart squid
