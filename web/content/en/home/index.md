---
title: 'WiFilter'
date: 2022-09-21T11:41:24+02:00
headless: true
---

A WiFi filter box wherever you go

To protect your children from adult content

Customizable with your rules, your choices

--

[Install](https://framagit.org/underxctrl/wifilter/-/blob/main/README.md#install)
