---
title: 'About'
image: 'access_denied.jpg'
weight: 8
---

<br>WiFilter allows filtering of any WiFi network based on a wireless router ([RaspAP](https://raspap.com/)) extended to run a proxy filter ([squidGuard](http://www.squidguard.org/)).<br>

By default, it filters adult sites based on the [Université Toulouse 1 Capitole blacklist](https://dsi.ut-capitole.fr/blacklists/index_en.php).<br>

WiFilter can even filter out adult sites that are not on the blacklist using [mitmproxy](https://mitmproxy.org/) and a machine learning model (trained on English and French adult sites, experimental).

WiFilter has been tested on Raspberry Pi 4 Model B and sets up in minutes.<br><br>

<span style='font-size: .75em'>Image adapted from https://supernova.eso.org/exhibition/images/0108_A_matrix/.<br>
Credit: ESO/Caroline Bachot.</span>

WiFilter's goal is to enable you to filter adult sites yourself from any WiFi network.

No need to register or connect to an online service. Running locally without any data leak.

The only requirement is to have a Raspberry Pi (tested on Raspberry Pi 4 Model B).

WiFilter comes in two flavors: basic and advanced (experimental).

### Basic system

![basic](/wifilter/home/basic_system.png)

The basic system is based on an adapted version of [RaspAP](https://raspap.com/) for wireless routing. It acts as a wireless access point itself connected to a WiFi network to access the web. It integrates [squidGuard](http://www.squidguard.org/) for filtering adult sites based on the [Université Toulouse 1 Capitole blacklist](https://dsi.ut-capitole.fr/blacklists/index_en.php).

The basic system can be installed by following [these instructions](https://framagit.org/underxctrl/wifilter/#install).

### Advanced system (experimental)

![advanced](/wifilter/home/advanced_system.png)

However, with approximately [200,000 newly registered domains per day](https://whoisds.com/newly-registered-domains), unknown adult sites can escape the basic system which relies solely on a list of domain names. To detect them, a dynamic content analysis is required. This analysis has been implemented as an extension of the core system using [mitmproxy](https://mitmproxy.org/). After the [squidGuard](http://www.squidguard.org/) filter, the textual content of the website is analysed to detect adult sites using a [mitmproxy](https://mitmproxy.org/) addon. This addon classifies the textual content of websites using a [scikit-learn](https://scikit-learn.org/) implementation of a linear support vector machine (SVM). This classifier has been trained on English and French adult sites: therefore, websites previously detected as being in another language by [langdetect](https://pypi.org/project/langdetect/) are filtered by the addon. This filtering can however be suspended for a domain name by declaring it in the white list.

The advanced system can be installed by following [these instructions](https://framagit.org/underxctrl/wifilter/#optional-advanced-system-experimental).
