---
title: 'WiFilter'
date: 2022-09-21T11:41:24+02:00
headless: true
---

Une boîtier filtre WiFi partout où vous allez

Pour protéger vos enfants des contenus pour adultes

Personnalisable avec vos règles, vos choix

--

[Installer](https://framagit.org/underxctrl/wifilter/-/blob/main/README.md#install)
