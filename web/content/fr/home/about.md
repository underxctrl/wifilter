---
title: 'A propos'
image: 'access_denied.jpg'
weight: 8
---

WiFilter permet de filtrer tout réseau WiFi et est basé sur un routeur sans fil ([RaspAP](https://raspap.com/)) intégrant un proxy filtrant ([squidGuard](http://www.squidguard.org/)).<br>

Par défaut, il filtre les sites adultes basés sur la [liste noire de l'Université Toulouse 1 Capitole](https://dsi.ut-capitole.fr/blacklists/).<br>

WiFilter peut également filtrer les sites pour adultes qui ne figurent pas sur la liste noire à l'aide de [mitmproxy](https://mitmproxy.org/) et d'un modèle d'apprentissage automatique (entraîné sur des sites pour adultes anglais et français, expérimental).

WiFilter a été testé sur Raspberry Pi 4 Model B et s'installe en quelques minutes.<br><br>

<span style='font-size: .75em'>Image adaptée de https://supernova.eso.org/exhibition/images/0108_A_matrix/.<br>
Crédit : ESO/Caroline Bachot.</span>

L'objectif de WiFilter est de vous permettre de filtrer vous-même les sites pour adultes à partir de n'importe quel réseau WiFi.

Pas besoin de s'enregistrer ou de se connecter à un service en ligne. Il fonctionne localement sans entraîner de fuite de données.

Le seul prérequis est d'avoir un Raspberry Pi (testé sur Raspberry Pi 4 Model B).

WiFilter existe en deux versions : basique et avancée (expérimental).

### Système de base

![basic](/wifilter/home/basic_system.png)

Le système de base repose sur une version adaptée de [RaspAP](https://raspap.com/) pour le routage sans fil. Il fonctionne comme un point d'accès sans fil lui-même connecté à un réseau WiFi pour accéder au web. Il intègre [squidGuard](http://www.squidguard.org/) pour le filtrage des sites adultes basé sur la [liste noire de l'Université Toulouse 1 Capitole](https://dsi.ut-capitole.fr/blacklists/).

Le système de base peut être installé en suivant [ces instructions](https://framagit.org/underxctrl/wifilter/#install).

### Système avancé (expérimental)

![advanced](/wifilter/home/advanced_system.png)

Cependant, avec environ [200 000 domaines nouvellement enregistrés par jour](https://whoisds.com/newly-registered-domains), des sites adultes inconnus peuvent échapper au système de base qui s'appuie uniquement sur une liste de noms de domaine. Pour les détecter, une analyse dynamique du contenu est nécessaire. Cette analyse a été implémentée en tant qu'extension du système de base en utilisant [mitmproxy](https://mitmproxy.org/). Après le filtre [squidGuard](http://www.squidguard.org/), le contenu textuel du site web est analysé pour détecter les sites pour adultes à l'aide d'un module d'extension de [mitmproxy](https://mitmproxy.org/). Ce module classifie le contenu textuel des sites web à l'aide d'une implémentation [scikit-learn](https://scikit-learn.org/) d'une machine à vecteurs de support (SVM) linéaire. Ce classifieur a été entraîné sur des sites pour adultes anglais et français : par conséquent, les sites web précédemment détectés comme étant dans une autre langue par [langdetect](https://pypi.org/project/langdetect/) sont filtrés par le module d'extension. Ce filtrage peut toutefois être suspendu pour un nom de domaine en le déclarant dans la liste blanche.

Le système avancé peut être installé en suivant [ces instructions](https://framagit.org/underxctrl/wifilter/#optional-advanced-system-experimental).
